# Eigen3

Linear algebra package. Contains version 3.3.9 of Eigen3 plus our own modifications to enable adjoint and transpose solve for Eigen::SparseLU. Furthermore, the /eigen3/unsupported/Eigen/src/NonLinearOptimization/HybridNonLinearSolver.h has been changed in order to be compatible with dco.


The upstream URL is http://eigen.tuxfamily.org.
